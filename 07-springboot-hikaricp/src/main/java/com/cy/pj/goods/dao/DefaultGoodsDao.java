package com.cy.pj.goods.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DefaultGoodsDao implements GoodsDao{
    @Autowired
    private DataSource datasource;
    public List<Map<String,Object>> findGoods() throws SQLException {
        Connection conn =null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql="select * from tb_goods";
        try {
            conn= datasource.getConnection();
            stmt=conn.createStatement();
            rs=stmt.executeQuery(sql);
            List<Map<String,Object>> list = new ArrayList<>();
            while (rs.next()){
//                Map<String,Object> map = new HashMap();
//                map.put("id",rs.getInt(1));
//                map.put("name", rs.getString(2));
//                map.put("remark", rs.getString(3));
//                map.put("createdTime", rs.getString(4));
//                list.add(map);
                list.add(rowMap(rs));
            }
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException(throwables);
        }finally{
            close(rs, stmt, conn);
        }
    }

    private Map<String, Object> rowMap(ResultSet rs) throws SQLException {
        Map<String,Object> rowMap = new HashMap<>();
        ResultSetMetaData rsmd = rs.getMetaData();//获取元数据；（描述数据的数据）
        int columnCount = rsmd.getColumnCount();
        for (int i = 1 ; i<=columnCount;i++){
            rowMap.put(rsmd.getColumnLabel(i), rs.getObject(rsmd.getColumnLabel(i)));
        }
        return rowMap;
    }
    private void close(ResultSet rs,Statement stmt,Connection conn){
        if(rs!=null)try{rs.close();}catch(Exception e){e.printStackTrace();}
        if(stmt!=null)try{stmt.close();}catch(Exception e){e.printStackTrace();}
        //这里的连接是返回到了池中
        if(conn!=null)try{conn.close();}catch(Exception e){e.printStackTrace();}
    }

}
