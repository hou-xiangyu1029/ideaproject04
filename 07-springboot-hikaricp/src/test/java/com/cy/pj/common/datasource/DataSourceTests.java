package com.cy.pj.common.datasource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;

@SpringBootTest
public class DataSourceTests {
    @Autowired
    private DataSource datasource;
    @Test
    void testConnection() throws Exception{
        Connection conn1=datasource.getConnection();
        System.out.println(conn1);
        conn1.close();
    }
}
