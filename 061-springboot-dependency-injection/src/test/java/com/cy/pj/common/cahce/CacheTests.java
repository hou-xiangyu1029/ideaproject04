package com.cy.pj.common.cahce;

import com.cy.pj.common.cache.Cache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheTests {
    @Autowired
    private Cache softCache;
    @Autowired
    @Qualifier("weakCache")
    private Cache cache;
    @Test
    void testCache(){
        System.out.println(softCache);
        System.out.println(cache);
    }
}
