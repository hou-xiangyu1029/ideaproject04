package com.cy.pj.comon.service;

import com.cy.pj.common.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CacheService {
    @Qualifier("softCache")
    @Autowired
    private Cache cahce;
    @Autowired
    public CacheService(@Qualifier("softCache") Cache cache){
        this.cahce=cache;
        System.out.println("CacheService()");
    }
}
