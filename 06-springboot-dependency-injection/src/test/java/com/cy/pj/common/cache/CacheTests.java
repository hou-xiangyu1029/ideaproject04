package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheTests {
    //@qualifier注解要与@Autowired注解配合使用。指定一个实现类注入
    @Qualifier("softCache")
    @Autowired      //在spring容器中，告诉其要注入这个bean。如果bean名与属性名相同，则直接注入
    private Cache cache;
    @Test
    void testCache(){
        System.out.println("cache");
    }
}
