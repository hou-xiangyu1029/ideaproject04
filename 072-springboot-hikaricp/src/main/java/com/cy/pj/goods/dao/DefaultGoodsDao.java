package com.cy.pj.goods.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Repository
public class DefaultGoodsDao implements GoodsDao{
    @Autowired
    private DataSource datasource;

    @Override
    public List<Map<String, Object>> findGoods() throws SQLException {
        Connection conn = null;
        Statement stat = null;
        ResultSet rs = null;
        String sql = "select * from tb_goods";
        try {
            conn=datasource.getConnection();
            stat =conn.createStatement();
            rs=stat.executeQuery(sql);
            List<Map<String,Object>> list = new ArrayList();
            while (rs.next()){
                list.add(rowMap(rs));
            }
            return list;
        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally{
            rs.close();
            stat.close();
            conn.close();
        }
    }

    private Map<String, Object> rowMap(ResultSet rs) throws SQLException {
        Map<String,Object> rowMap = new HashMap<>();
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        for (int i = 1; i <=columnCount ; i ++){
            rowMap.put(rsmd.getColumnLabel(i), rs.getObject(rsmd.getColumnLabel(i)));
        }
        return rowMap;
    }
}
