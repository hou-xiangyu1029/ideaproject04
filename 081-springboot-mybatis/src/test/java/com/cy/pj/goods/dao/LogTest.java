package com.cy.pj.goods.dao;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class LogTest {
    private static final Logger log= LoggerFactory.getLogger(LogTest.class);
    @Test
    void testLogLevel(){
        log.trace("log.level=trace");
        log.info("log.level=info");
        log.debug("log.level=debug");

    }
}
