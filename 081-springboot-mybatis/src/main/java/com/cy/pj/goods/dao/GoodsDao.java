package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper//此注解用于描述mybats框架中的数据逻辑层接口；
public interface GoodsDao {

    List<Goods> findGoods();
}
