package com.cy.pj.common.pool;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/*构建一个对象池类型，将此类型的对象交给spring管理*/
//@Lazy
@Component
@Scope("prototype")
public class ObjectPool {
/*定义无参构造，目的监控何时创建此对象*/
    public ObjectPool(){
        System.out.println("ObjectPool()");
    }
}
