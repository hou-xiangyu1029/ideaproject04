package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*springboot在启动时做了什么呢？
* 1）从磁盘中的指定位置将类读到内存，可以基于jvm类加载参数进行分析
* 2）读取类的配置信息，并基于对象配置信息
* 3）基于配置信息初始化spring资源，并创建bean的实例进行存储和管理。
*
* */




@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
